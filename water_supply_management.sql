-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2020 at 05:06 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `water_supply_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `for_month_of` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `complain`
--

CREATE TABLE `complain` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `date` date NOT NULL,
  `contact_number` bigint(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(45) NOT NULL,
  `contact` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(33),
(33),
(33),
(33),
(33),
(33);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `citizenship_no` bigint(10) NOT NULL,
  `image` varchar(45) NOT NULL DEFAULT 'empty_image.png',
  `phone` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `citizenship_no`, `image`, `phone`) VALUES
(1, 'sadikshya', 'maya', 'shrestha', 'sadik12@gmail.com', 123, 'abcd.png', 12333),
(2, 'bilson', 'kumar', 'naga', 'naga12@gmail.com', 101, 'empty_image.png', 11343453),
(17, 'Sadikshya', 'Maya', 'Shrestha', 'sadik@gmail.com', 12323, 'test.png', 3432),
(18, 'Sadikshya', 'Cutie', 'Shrestha', 'kidas@gmail.com', 1212, 'test.png', 323424),
(19, 'harry', 'lal', 'sharma', 'harry@gmail.com', 177, 'test.png', 456456),
(20, 'SHyam', 'Lal', 'Sharam', 'shyam@gmail.com', 565, 'test.png', 98989),
(21, 'Shyam', 'ram ', 'prasad', 'sim@gmail.com', 2121, 'test.png', 67768),
(22, 'Hari', 'Lal', 'prasad', 'hari@gmail.com', 2323, 'test.png', 645645),
(23, 'Shyam', 'Prasad', 'Raman', 'shyam@gmail.com', 212190, 'test.png', 234324234),
(24, 'test3', 'test', 'best', 'test@best.com', 121212, 'test.png', 45456456),
(29, 'adam', 'best', 'west', 'adam@adam.com', 23, 'test.png', 989),
(31, 'sita', 'nanu', 'karki', 'sita@gmail.com', 786, 'test.png', 12345);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`id`, `user_name`, `password`, `user_id`) VALUES
(12, 'bilson', '$2a$10$uLP0KRlZGN4omdoLNqiN8eygbs/TkpYqAnJl8OzGe.Xi847qXa07m', 2),
(30, 'adam', '$2a$10$cpPVYgKg9biRCJYZltHsuOr5aGLeemw1hiqeaVieoMO1lQo1uJDOy', 29),
(32, 'sita', '$2a$10$4f2YbexVsRdLdWrBJ/8pkes9Jf.H0CT4VgsKlVOJ6JqKy.VE7lLOW', 31);

-- --------------------------------------------------------

--
-- Table structure for table `water_record`
--

CREATE TABLE `water_record` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `litre` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `water_record`
--

INSERT INTO `water_record` (`id`, `description`, `litre`) VALUES
(1, 'water tank1', 5000),
(8, 'water tank8', 1000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD UNIQUE KEY `user_name_UNIQUE` (`user_name`),
  ADD UNIQUE KEY `UK_i1dw3k8qw5770uqdkwb1mn46s` (`user_id`),
  ADD KEY `fk_admin_login_user1_idx` (`user_id`);

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_3ac38309d899hoderihd5mh9j` (`user_id`),
  ADD KEY `fk_Bill_user1_idx` (`user_id`);

--
-- Indexes for table `complain`
--
ALTER TABLE `complain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `citizenship_no_UNIQUE` (`citizenship_no`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD UNIQUE KEY `user_name_UNIQUE` (`user_name`),
  ADD UNIQUE KEY `UK_3fmuuqnycdbyuxsew3r26cg34` (`user_id`),
  ADD KEY `fk_user_login_user_idx` (`user_id`);

--
-- Indexes for table `water_record`
--
ALTER TABLE `water_record`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complain`
--
ALTER TABLE `complain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `water_record`
--
ALTER TABLE `water_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD CONSTRAINT `fk_admin_login_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `fk_Bill_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_login`
--
ALTER TABLE `user_login`
  ADD CONSTRAINT `fk_user_login_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
