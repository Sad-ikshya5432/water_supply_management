/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.services;

import com.watersupply.watersupply.dto.ComplainDto;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface ComplainService {
    public List<ComplainDto> getAllComplain();
    public ComplainDto saveComplain(ComplainDto complain);
    
}
