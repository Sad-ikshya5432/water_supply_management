/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.services;

import com.watersupply.watersupply.dto.UserLoginDto;

/**
 *
 * @author Lenovo
 */
public interface UserLoginService {
    public UserLoginDto saveUserLogin(UserLoginDto userLogin);
    public UserLoginDto getByUserName(String username);
    
}
