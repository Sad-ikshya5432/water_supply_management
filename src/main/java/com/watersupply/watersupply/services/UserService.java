/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.services;

import com.watersupply.watersupply.dto.UserDto;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface UserService {
    public List<UserDto> getAllUser();
    public UserDto saveUser(UserDto user);
    public UserDto getUserById(Integer id);
    public UserDto editUser(UserDto user);
    public void deleteUser(Integer id);
    
}
