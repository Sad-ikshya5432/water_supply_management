/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.services;

import com.watersupply.watersupply.dto.WaterRecordDto;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lenovo
 */
public interface WaterRecordService {
   public List<WaterRecordDto> getAllWater();
   public WaterRecordDto saveWater(WaterRecordDto water);
   public WaterRecordDto editWater(WaterRecordDto water);
   
    
}
