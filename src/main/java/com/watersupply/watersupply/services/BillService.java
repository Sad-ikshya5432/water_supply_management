/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.services;

import com.watersupply.watersupply.dto.BillDto;

/**
 *
 * @author Lenovo
 */
public interface BillService {
    public BillDto getBillById(Integer id);
    public BillDto getByMonthOf(String forMonthOf);
    public BillDto saveBill(BillDto bill);
    
}
