/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.controllers;

import com.watersupply.watersupply.dto.WaterRecordDto;
import com.watersupply.watersupply.services.WaterRecordService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lenovo
 */
@RestController
@RequestMapping(value="/water")
public class WaterRecordController {
    @Autowired
    WaterRecordService waterService;
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public List<WaterRecordDto> getAllWater()
    {
        return waterService.getAllWater();
    }
    
    @RequestMapping(value="/save",method=RequestMethod.POST)
    public WaterRecordDto saveWater(@RequestBody WaterRecordDto water)
    {
        return waterService.saveWater(water);
        
    }
     @RequestMapping(value="/edit", method=RequestMethod.POST)
    public WaterRecordDto editWater(@RequestBody WaterRecordDto water)
    {
        return waterService.editWater(water);
    }
    
    
}
