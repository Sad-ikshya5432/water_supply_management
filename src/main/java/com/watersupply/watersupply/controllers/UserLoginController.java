/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.controllers;

import com.watersupply.watersupply.dto.UserLoginDto;
import com.watersupply.watersupply.services.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lenovo
 */

@RestController
@RequestMapping(value="/userLogin")
public class UserLoginController {
    @Autowired
    UserLoginService userLoginService;
    
    @RequestMapping(value="/save",method=RequestMethod.POST)
    public UserLoginDto saveUserLogin( @RequestBody UserLoginDto userLogin)
    {
        return userLoginService.saveUserLogin(userLogin);
    }
    @RequestMapping(value="/getByUsername",method=RequestMethod.GET)
    public UserLoginDto getByUsername(@RequestParam String username)
    {
        return userLoginService.getByUserName(username);
    }
    
}
