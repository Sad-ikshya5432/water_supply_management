/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.controllers;

import com.watersupply.watersupply.dto.ComplainDto;
import com.watersupply.watersupply.services.ComplainService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lenovo
 */
@RestController
@RequestMapping(value="/complain")
public class ComplainController {
    @Autowired
    ComplainService complainService;
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public List<ComplainDto> getAllComplain()
    {
        return complainService.getAllComplain();
    }
    @RequestMapping(value="/save", method=RequestMethod.POST)
    public ComplainDto saveComplain( @RequestBody ComplainDto complain)
    {
        return complainService.saveComplain(complain);
    }
    
    
}
