/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.controllers;

import com.watersupply.watersupply.dto.UserDto;
import com.watersupply.watersupply.services.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lenovo
 */
@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    @RequestMapping(value="/list",method= RequestMethod.GET)
    public List<UserDto> getUsers()
    {
        return userService.getAllUser();
    }
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public UserDto getUserById(@PathVariable Integer id)
    {
        return userService.getUserById(id);
    }
    @RequestMapping(value="/save", method=RequestMethod.POST)
    public UserDto saveUser(@RequestBody UserDto user)
    {
        return userService.saveUser(user);
    }
    @RequestMapping(value="/edit", method=RequestMethod.POST)
    public UserDto editUser(@RequestBody UserDto user)
    {
        return userService.editUser(user);
    }
    @RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
    public void deleteUser(@PathVariable Integer id)
    {
        userService.deleteUser(id);
    }
}
