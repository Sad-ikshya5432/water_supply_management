/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.controllers;

import com.watersupply.watersupply.dto.BillDto;
import com.watersupply.watersupply.services.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lenovo
 */
@RestController
@RequestMapping("/bill")
public class BillController {
    @Autowired
    BillService billService;
    
    @RequestMapping(value="/{id}" , method=RequestMethod.GET)
    public BillDto getBillById(@PathVariable Integer id)
    {
        return billService.getBillById(id);
    }
    @RequestMapping(value="/month/{month}", method=RequestMethod.GET)
    public BillDto getBillByForMonthOf(@PathVariable String forMonthOf)
    {
        return billService.getByMonthOf(forMonthOf);
    }
    @RequestMapping(value="/save",method =RequestMethod.POST)
            public BillDto saveBill(BillDto bill)
            {
                return billService.saveBill(bill);
              }
    }
    
    

