/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.impl;

import com.watersupply.watersupply.dto.UserLoginDto;
import com.watersupply.watersupply.entities.User;
import com.watersupply.watersupply.entities.UserLogin;
import com.watersupply.watersupply.repositories.UserLoginRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sadikshya
 */
@Service
 public class JwtUserDetailsService implements UserDetailsService {
     @Autowired
     private UserLoginRepository userLoginRepo;
     
     @Autowired
     private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserLogin userLogin=userLoginRepo.findByUserName(userName);
        
        if(userLogin==null){
            throw new UsernameNotFoundException("User not found with email:"+userName);
            
        }
        return new org.springframework.security.core.userdetails.User(userLogin.getUserName(), userLogin.getPassword(), new ArrayList<>());
        
    }
    public UserLogin save(UserLoginDto userLogin){
     User user=User.builder().id(userLogin.getUserId().getId()).build();
        UserLogin userLoginEntity=UserLogin.builder().id(userLogin.getId())
                                    .userName(userLogin.getUserName())
                                    .password(bcryptEncoder.encode(userLogin.getPassword()))
                                    .userId(user).build();
        return userLoginRepo.save(userLoginEntity);
    }

   
}
