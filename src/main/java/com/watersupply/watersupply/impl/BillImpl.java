/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.impl;

import com.watersupply.watersupply.dto.BillDto;
import com.watersupply.watersupply.dto.UserDto;
import com.watersupply.watersupply.entities.Bill;
import com.watersupply.watersupply.entities.User;
import com.watersupply.watersupply.repositories.BillRepository;
import com.watersupply.watersupply.repositories.UserRepository;
import com.watersupply.watersupply.services.BillService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lenovo
 */
@Service
public class BillImpl implements BillService{
    @Autowired
    BillRepository billRepo;
    UserRepository userRepo;
    

    @Override
    public BillDto getBillById(Integer id) {
        Bill bill=billRepo.findById(id).get();
        User user=userRepo.findById(bill.getUserId().getId()).get();
        UserDto userdto=UserDto.builder().id(user.getId())
                                        .firstName(user.getFirstName())
                                        .middleName(user.getMiddleName())
                                        .lastName(user.getLastName())
                                        .email(user.getEmail())
                                        .citizenshipNo(user.getCitizenshipNo())
                                        .build();
         return BillDto.builder().id(bill.getId())
                    .forMonthOf(bill.getForMonthOf())
                    .amount(bill.getAmount())
                    .userId(userdto)
                    .build();
         
    }

    @Override
    public BillDto getByMonthOf(String forMonthOf) {
        Bill bill=billRepo.findByForMonthOf(forMonthOf);
        UserDto user=UserDto.builder().id(bill.getUserId().getId())
                                       .firstName(bill.getUserId().getFirstName())
                                               .build();
        return BillDto.builder().id(bill.getId())
                                .forMonthOf(bill.getForMonthOf())
                                .userId(user)
                                   .amount(bill.getAmount()).build();
    }

    @Override
    public BillDto saveBill(BillDto bill) {
        User user=User.builder().id(bill.getUserId().getId()).build();
        Bill billEntity=Bill.builder().id(bill.getId())
                                .userId(user)
                                    .forMonthOf(bill.getForMonthOf())
                                    .amount(bill.getAmount())
                                    .build();
        billEntity =billRepo.save(billEntity);
        bill.setId(billEntity.getId());
        return bill;
        
    }
    
    
}
