/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.impl;

import com.watersupply.watersupply.dto.UserDto;
import com.watersupply.watersupply.entities.User;
import com.watersupply.watersupply.repositories.UserRepository;
import com.watersupply.watersupply.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lenovo
 */
@Service
public class UserImpl implements UserService{
    @Autowired
    UserRepository userRepo;

    @Override
    public List<UserDto> getAllUser() {
         List<User> user= userRepo.findAll();
        List<UserDto> userDtoList= new ArrayList<>();
        for(User u: user)
        {
          
            UserDto userDto=UserDto.builder().id(u.getId())
                            .firstName(u.getFirstName())
                            .middleName(u.getMiddleName())
                            .lastName(u.getLastName())
                            .email(u.getEmail())
                            .phone(u.getPhone())
                            .citizenshipNo(u.getCitizenshipNo())
                            .image(u.getImage()).build();
                            
            
            
            userDtoList.add(userDto);
        }
        return userDtoList;
       
    }

    @Override
    public UserDto saveUser(UserDto user) {
        User userEntity = User.builder().id(user.getId())
                           .firstName(user.getFirstName())
                            .middleName(user.getMiddleName())
                            .lastName(user.getLastName())
                            .email(user.getEmail())
                            .phone(user.getPhone())
                            .citizenshipNo(user.getCitizenshipNo())
                            .image(user.getImage()) .build();
        userEntity=userRepo.save(userEntity);
        user.setId(userEntity.getId());
        return user;
    }

    @Override
    public UserDto getUserById(Integer id) {
         User user=userRepo.findById(id).get();
        return UserDto.builder().id(user.getId())
                            .firstName(user.getFirstName())
                            .middleName(user.getMiddleName())
                            .lastName(user.getLastName())
                            .email(user.getEmail())
                            .phone(user.getPhone())
                            .citizenshipNo(user.getCitizenshipNo())
                            .image(user.getImage()).build();
    }

    @Override
    public UserDto editUser(UserDto user) {
        User userEntity = User.builder().id(user.getId())
                            .firstName(user.getFirstName())
                            .middleName(user.getMiddleName())
                            .lastName(user.getLastName())
                            .email(user.getEmail())
                            .phone(user.getPhone())
                            .citizenshipNo(user.getCitizenshipNo())
                            .image(user.getImage()).build();
        userEntity=userRepo.saveAndFlush(userEntity);
        user.setId(userEntity.getId());
        return user;
    }

    @Override
    public void deleteUser(Integer id) {
        userRepo.deleteById(id);
    }
    
}
