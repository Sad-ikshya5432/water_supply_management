/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.impl;

import com.watersupply.watersupply.dto.ComplainDto;
import com.watersupply.watersupply.entities.Complain;
import com.watersupply.watersupply.repositories.ComplainRepository;
import com.watersupply.watersupply.services.ComplainService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lenovo
 */
@Service
public class ComplainImpl implements ComplainService{
    @Autowired
    ComplainRepository complainRepo;

    @Override
    public List<ComplainDto> getAllComplain() {
        List<Complain> complain=complainRepo.findAll();
        List<ComplainDto> complainDtoList=new ArrayList<>();
        for(Complain c:complain)
        {
            ComplainDto complainDto=ComplainDto.builder().id(c.getId())
                                    .name(c.getName())
                                    .date(c.getDate())
                                    .contact(c.getContact())
                                    .description(c.getDescription())
                                    .location(c.getLocation()).build();
            
            complainDtoList.add(complainDto);
        }
        return complainDtoList;
    }

    @Override
    public ComplainDto saveComplain(ComplainDto complain) {
        Complain complainEntity=Complain.builder().id(complain.getId())
                                .name(complain.getName())
                                    .date(complain.getDate())
                                    .contact(complain.getContact())
                                    .description(complain.getDescription())
                                    .location(complain.getLocation()).build();
        
                complainEntity=complainRepo.save(complainEntity);
                complain.setId(complainEntity.getId());
                return complain;
    }
    
}
