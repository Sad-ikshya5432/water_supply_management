/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.impl;

import com.watersupply.watersupply.dto.WaterRecordDto;
import com.watersupply.watersupply.entities.WaterRecord;
import com.watersupply.watersupply.repositories.WaterRecordRepository;
import com.watersupply.watersupply.services.WaterRecordService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lenovo
 */
@Service
public class WaterRecordImpl implements WaterRecordService {
    @Autowired
    WaterRecordRepository waterRepo;

    @Override
    public List<WaterRecordDto> getAllWater() {
        List<WaterRecord> water=waterRepo.findAll();
        List<WaterRecordDto> waterDtoList=new ArrayList<>();
        for(WaterRecord w: water)
        {
            WaterRecordDto waterDto=WaterRecordDto.builder().id(w.getId())
                                     .description(w.getDescription())
                                     .litre(w.getLitre()).build();
            
            waterDtoList.add(waterDto);
        }
        return waterDtoList;
        
        
    }

    @Override
    public WaterRecordDto saveWater(WaterRecordDto water) {
        WaterRecord waterEntity=WaterRecord.builder().id(water.getId())
                                .description(water.getDescription())
                                .litre(water.getLitre()).build();
        
        waterEntity=waterRepo.save(waterEntity);
        water.setId(waterEntity.getId());
        return water;
    }

    @Override
    public WaterRecordDto editWater(WaterRecordDto water) {
        WaterRecord waterEntity=WaterRecord.builder().id(water.getId())
                                .description(water.getDescription())
                                .litre(water.getLitre()).build();
        
        waterEntity=waterRepo.saveAndFlush(waterEntity);
        water.setId(waterEntity.getId());
        return water;
    }
    
    
}
