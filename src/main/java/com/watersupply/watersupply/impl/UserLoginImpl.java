/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.impl;

import com.watersupply.watersupply.dto.UserDto;
import com.watersupply.watersupply.dto.UserLoginDto;
import com.watersupply.watersupply.entities.User;
import com.watersupply.watersupply.entities.UserLogin;
import com.watersupply.watersupply.repositories.UserLoginRepository;
import com.watersupply.watersupply.repositories.UserRepository;
import com.watersupply.watersupply.services.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lenovo
 */
@Service
public class UserLoginImpl implements UserLoginService {
    @Autowired
    UserLoginRepository userLoginRepo;
    @Autowired
    UserRepository userRepo;
    

    @Override
    public UserLoginDto saveUserLogin(UserLoginDto userLogin) {
        User user=User.builder().id(userLogin.getUserId().getId()).build();
        UserLogin userLoginEntity=UserLogin.builder().id(userLogin.getId())
                                    .userName(userLogin.getUserName())
                                    .password(userLogin.getPassword())
                                    .userId(user).build();
        
        userLoginEntity=userLoginRepo.save(userLoginEntity);
        userLogin.setId(userLoginEntity.getId());
        return userLogin;
    }

    @Override
    public UserLoginDto getByUserName(String username) {
        UserLogin userLogin=userLoginRepo.findByUserName(username);
        int id=userLogin.getUserId().getId();
        User userEntity=userRepo.findById(id).get();
        System.out.println("userLogin"+userEntity);
        UserDto user=UserDto.builder().id(userLogin.getUserId().getId())
                                       .firstName(userEntity.getFirstName())
                                         . middleName(userEntity.getMiddleName())  
                                        . lastName(userEntity.getLastName())
                                        .email(userEntity.getEmail())
                                        .phone(userEntity.getPhone())
                                        .citizenshipNo(userEntity.getCitizenshipNo())
                                        .image(userEntity.getImage()).build();
        return UserLoginDto.builder().id(userLogin.getId())
                                    .userName(userLogin.getUserName())
                                    .password(userLogin.getPassword())
                                    .userId(user).build();
    }
    
}
