/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watersupply.watersupply.repositories;

import com.watersupply.watersupply.entities.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Lenovo
 */
public interface UserLoginRepository extends JpaRepository<UserLogin,Integer>{
    UserLogin findByUserName(String userName);
}
