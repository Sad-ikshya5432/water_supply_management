package com.watersupply.watersupply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WatersupplyApplication {

	public static void main(String[] args) {
		SpringApplication.run(WatersupplyApplication.class, args);
	}

}
